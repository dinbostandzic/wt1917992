let assert = chai.assert;
describe('Tabela', function() {
 describe('iscrtajKalendar()', function() {
   it('pozivanje za mjesec sa 30 dana', function() {
     Kalendar.iscrtajKalendar(document.getElementById("kalendar"),3);
     var k=0;
     for(var i=0;i<42;i++){
        if(!(document.getElementsByClassName("mala")[i].classList.contains("nevidljiva")))
        {k++;}
     }
     assert.equal(k, 30,"Broj vidljivih celija treba biti 30");
   });
});
   describe('iscrtajKalendar()', function() {
    it('pozivanje za mjesec sa 31 dana', function() {
      Kalendar.iscrtajKalendar(document.getElementById("kalendar"),0);
      var k=0;
      for(var i=0;i<42;i++){
         if(!(document.getElementsByClassName("mala")[i].classList.contains("nevidljiva")))
         {k++;}
      }
      assert.equal(k, 31,"Broj vidljivih celija treba biti 31");
    });
});
describe('iscrtajKalendar()', function() {
    it('pozivanje za trenutni mjesec', function() {
      Kalendar.iscrtajKalendar(document.getElementById("kalendar"),10);
      var k=0;
      for(var i=0;i<42;i++){
         if((document.getElementsByClassName("mala")[i].classList.contains("nevidljiva")))
         {k++;}
         else 
         {
            k++;
            break;
            }
      }
      assert.equal(k, 5,"Prva vidljiva celija treba biti na 5.-om mjestu");
    });
});
describe('iscrtajKalendar()', function() {
    it('pozivanje za trenutni mjesec', function() {
      Kalendar.iscrtajKalendar(document.getElementById("kalendar"),10);
      var k=0;
      for(var i=0;i<42;i++){
        k++;
         if((document.getElementsByClassName("gornja")[i].innerHTML==30))
            {
                break;
            }
         }
      assert.equal(k%7, 6,"Celija koja sadrzi broj 30 treba se nalaziti u 6.-oj koloni");
    });
});

describe('iscrtajKalendar()', function() {
    it('pozivanje za januar', function() {
      Kalendar.iscrtajKalendar(document.getElementById("kalendar"),0);
      var k=0;
      for(var i=0;i<42;i++){
         if((document.getElementsByClassName("mala")[i].classList.contains("nevidljiva")))
         {k++;}
         else 
         {
            k++;
            break;
            }
      }
      var t=1;
      var tacno=1;
      for(var i=1;i<42;i++){
        if(!(document.getElementsByClassName("mala")[i].classList.contains("nevidljiva"))){
            if((document.getElementsByClassName("gornja")[i].innerHTML==t))
            {
                t++;
            }
            else{
                tacno=0;
            }
        }
      }
      assert.equal(k, 2,"Prva vidljiva celija treba biti na 2.-om mjestu");
      assert.equal(tacno, 1,"Da je pobrkan redoslijed dana varijabla tacno bi postala 0");
    });
});
describe('iscrtajKalendar()', function() {
    it('pozivanje za februar', function() {
      Kalendar.iscrtajKalendar(document.getElementById("kalendar"),1);
      var k=0;
      for(var i=0;i<42;i++){
         if(!(document.getElementsByClassName("mala")[i].classList.contains("nevidljiva")))
         {k++;}
      }
      assert.equal(k, 28,"Broj vidljivih celija treba biti 28");
    });
});
describe('iscrtajKalendar()', function() {
    it('pozivanje za maj', function() {
      Kalendar.iscrtajKalendar(document.getElementById("kalendar"),4);
      var k=0;
      for(var i=0;i<42;i++){
         if((document.getElementsByClassName("mala")[i].classList.contains("nevidljiva")))
         {k++;}
         else 
         {
            k++;
            break;
            }
      }
      assert.equal(k, 3,"Prva vidljiva celija treba biti na 3.-em mjestu");
    });
});
describe('obojiZauzeca()', function() {
    it('podaci nisu ucitani', function() {
      Kalendar.ucitajPodatke([],[]);
      Kalendar.obojiZauzeca(document.getElementById("kalendar"),4,"VA1","12:00","13:00");
      var k=1;
      for(var i=0;i<42;i++){
         if(!(document.getElementsByClassName("mala")[i].classList.contains("nevidljiva")) && document.getElementsByClassName("mala")[i].children[0].children[1].children[0].classList=="zauzeta")
         k=0;
      assert.equal(k, 1,"Da postoji ijedna zauzeta celija k bi postalo 0");
      }
    });
});
describe('obojiZauzeca()', function() {
    it('pozivanje sa duplim zauzecima', function() {
      Kalendar.ucitajPodatke([],[{datum: "10.05.2019", pocetak: "09:00", kraj: "16:00", naziv: "VA1", predavac: "blabla"},{datum: "10.05.2019", pocetak: "09:00", kraj: "16:00", naziv: "VA1", predavac: "blabla"}]);
      Kalendar.obojiZauzeca(document.getElementById("kalendar"),4,"VA1","12:00","13:00");
      var k=1;
      for(var i=0;i<42;i++){
         if(!(document.getElementsByClassName("mala")[i].classList.contains("nevidljiva")))
         break;
      }
      if(document.getElementsByClassName("mala")[i+10].children[0].children[1].children[0].classList=="zauzeta")
      k=0;
      assert.equal(k, 1,"Da je 10. dan u maju ostao zelen k bi postalo 0");
      
    });
});
describe('obojiZauzeca()', function() {
    it('pozivanje sa periodicnim zauzecem za drugi semestar', function() {
      Kalendar.ucitajPodatke([{dan:0, semestar: "zimski", pocetak:"09:00", kraj:"20:00", naziv: "VA1", predavac: "blabla" }],[]);
      Kalendar.obojiZauzeca(document.getElementById("kalendar"),4,"VA1","12:00","13:00");
      var k=1;
      for(var i=0;i<42;i++){
         if(!(document.getElementsByClassName("mala")[i].classList.contains("nevidljiva")) && document.getElementsByClassName("mala")[i].children[0].children[1].children[0].classList=="zauzeta")
         k=0;
      }
      assert.equal(k, 1,"Da postoji ijedna zauzeta celija k bi postalo 0");
      
    });
});
describe('obojiZauzeca()', function() {
    it('pozivanje sa zauzecem za drugi mjesec', function() {
      Kalendar.ucitajPodatke([],[{datum: "10.06.2019", pocetak: "09:00", kraj: "16:00", naziv: "VA1", predavac: "blabla"}]);
      Kalendar.obojiZauzeca(document.getElementById("kalendar"),4,"VA1","12:00","13:00");
      var k=1;
      for(var i=0;i<42;i++){
         if(!(document.getElementsByClassName("mala")[i].classList.contains("nevidljiva")) && document.getElementsByClassName("mala")[i].children[0].children[1].children[0].classList=="zauzeta")
         k=0;
      }
      assert.equal(k, 1,"Da postoji ijedna zauzeta celija k bi postalo 0");
      
    });
});
describe('obojiZauzeca()', function() {
    it('pozivanje sa zauzecem za drugi mjesec', function() {
      Kalendar.ucitajPodatke([{dan:0, semestar: "ljetni", pocetak:"09:00", kraj:"20:00", naziv: "VA1", predavac: "blabla" },
      {dan:1, semestar: "ljetni", pocetak:"09:00", kraj:"20:00", naziv: "VA1", predavac: "blabla" },
      {dan:2, semestar: "ljetni", pocetak:"09:00", kraj:"20:00", naziv: "VA1", predavac: "blabla" },
      {dan:3, semestar: "ljetni", pocetak:"09:00", kraj:"20:00", naziv: "VA1", predavac: "blabla" },
      {dan:4, semestar: "ljetni", pocetak:"09:00", kraj:"20:00", naziv: "VA1", predavac: "blabla" },
      {dan:5, semestar: "ljetni", pocetak:"09:00", kraj:"20:00", naziv: "VA1", predavac: "blabla" },
      {dan:6, semestar: "ljetni", pocetak:"09:00", kraj:"20:00", naziv: "VA1", predavac: "blabla" }],[]);
      Kalendar.obojiZauzeca(document.getElementById("kalendar"),4,"VA1","12:00","13:00");
      var k=1;
      for(var i=0;i<42;i++){
         if(!(document.getElementsByClassName("mala")[i].classList.contains("nevidljiva")) && document.getElementsByClassName("mala")[i].children[0].children[1].children[0].classList=="slobodna")
         k=0;
      }
      assert.equal(k, 1,"Da postoji ijedna slobodna celija k bi postalo 0");
      
    });
});
describe('obojiZauzeca()', function() {
    it('pozivanje funkcije 2 puta uzastopno', function() {
      Kalendar.ucitajPodatke([],[{datum: "10.05.2019", pocetak: "09:00", kraj: "16:00", naziv: "VA1", predavac: "blabla"},{datum: "10.05.2019", pocetak: "09:00", kraj: "16:00", naziv: "VA1", predavac: "blabla"}]);
      Kalendar.obojiZauzeca(document.getElementById("kalendar"),4,"VA1","12:00","13:00");
      var k=1;
      for(var i=0;i<42;i++){
         if(!(document.getElementsByClassName("mala")[i].classList.contains("nevidljiva")))
         break;
      }
      if(document.getElementsByClassName("mala")[i+10].children[0].children[1].children[0].classList=="zauzeta")
      k=0;
      assert.equal(k, 1,"Da je 10. dan u maju ostao zelen k bi postalo 0");
      Kalendar.obojiZauzeca(document.getElementById("kalendar"),4,"VA1","12:00","13:00");
      var k=1;
      for(var i=0;i<42;i++){
         if(!(document.getElementsByClassName("mala")[i].classList.contains("nevidljiva")))
         break;
      }
      if(document.getElementsByClassName("mala")[i+10].children[0].children[1].children[0].classList=="zauzeta")
      k=0;
      assert.equal(k, 1,"Da je 10. dan u maju ostao zelen k bi postalo 0");
      
    });
});
describe('obojiZauzeca()', function() {
    it('pozivanje ucitajpodatke oboji ucitajpodatke sa drugim podacima pa opet oboji', function() {
      Kalendar.ucitajPodatke([{dan:0, semestar: "ljetni", pocetak:"09:00", kraj:"20:00", naziv: "VA1", predavac: "blabla" },
      {dan:1, semestar: "ljetni", pocetak:"09:00", kraj:"20:00", naziv: "VA1", predavac: "blabla" },
      {dan:2, semestar: "ljetni", pocetak:"09:00", kraj:"20:00", naziv: "VA1", predavac: "blabla" },
      {dan:3, semestar: "ljetni", pocetak:"09:00", kraj:"20:00", naziv: "VA1", predavac: "blabla" },
      {dan:4, semestar: "ljetni", pocetak:"09:00", kraj:"20:00", naziv: "VA1", predavac: "blabla" },
      {dan:5, semestar: "ljetni", pocetak:"09:00", kraj:"20:00", naziv: "VA1", predavac: "blabla" },
      {dan:6, semestar: "ljetni", pocetak:"09:00", kraj:"20:00", naziv: "VA1", predavac: "blabla" }],[]);
      Kalendar.obojiZauzeca(document.getElementById("kalendar"),4,"VA1","12:00","13:00");
      Kalendar.ucitajPodatke([],[{datum: "10.05.2019", pocetak: "09:00", kraj: "16:00", naziv: "VA1", predavac: "blabla"},{datum: "10.05.2019", pocetak: "09:00", kraj: "16:00", naziv: "VA1", predavac: "blabla"}]);
      Kalendar.obojiZauzeca(document.getElementById("kalendar"),4,"VA1","12:00","13:00");
      var k=1;
      for(var i=0;i<42;i++){
         if(!(document.getElementsByClassName("mala")[i].classList.contains("nevidljiva")))
         break;
      }
      if(document.getElementsByClassName("mala")[i].children[0].children[1].children[0].classList=="zauzeta")
      k=0;
      assert.equal(k, 1,"Drugo bojenje boji samo 10. maj kad bi prvi dan bio zauzet to bi znacilo da nesto nije uredu i k bi postalo 0");
      
    });
});
describe('obojiZauzeca()', function() {
    it('bojenje jedne sedmice u maju', function() {
      Kalendar.ucitajPodatke([{dan:0, semestar: "ljetni", pocetak:"09:00", kraj:"20:00", naziv: "VA1", predavac: "blabla" }],[]);
      Kalendar.obojiZauzeca(document.getElementById("kalendar"),4,"VA1","12:00","13:00");
      var k=1;
      for(var i=0;i<42;i++){
         if(!(document.getElementsByClassName("mala")[i].classList.contains("nevidljiva")) && i%7==0 && document.getElementsByClassName("mala")[i].children[0].children[1].children[0].classList=="slobodna")
         k=0;
      }
      assert.equal(k, 1,"Da postoji ijedna slobodna celija u ponedeljak k bi postalo 0");
      
    });
});
    describe('obojiZauzeca()', function() {
        it('bojenje jedne sedmice i jednog dana u maju', function() {
          Kalendar.ucitajPodatke([{dan:0, semestar: "ljetni", pocetak:"09:00", kraj:"20:00", naziv: "VA1", predavac: "blabla" }],[{datum: "10.05.2019", pocetak: "09:00", kraj: "16:00", naziv: "VA1", predavac: "blabla"}]);
          Kalendar.obojiZauzeca(document.getElementById("kalendar"),4,"VA1","12:00","13:00");
          var k=1;
          for(var i=0;i<42;i++){
             if(!(document.getElementsByClassName("mala")[i].classList.contains("nevidljiva")) && i%7==0 && document.getElementsByClassName("mala")[i].children[0].children[1].children[0].classList=="slobodna")
             k=0;
          }
          for(var i=0;i<42;i++){
            if(!(document.getElementsByClassName("mala")[i].classList.contains("nevidljiva")))
            break;
         }
         if(document.getElementsByClassName("mala")[i+10].children[0].children[1].children[0].classList=="zauzeta"){ 
            k=0;
         }
          assert.equal(k, 1,"Da postoji ijedna slobodna celija u ponedeljak ili da je 10.05 slobodan k bi postalo 0");
          
        });
});
});
