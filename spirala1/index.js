var proba = [];
var doklesam = 0;
var content;
var poceci3 = [2, 5, 6, 2, 4, 0, 2, 5, 1, 3, 6, 1];
const express = require("express");
const bodyParser = require("body-parser");
const fs = require("fs");
const app = express();
const db = require("./db.js");
db.sequelize.sync({ force: true }).then(function() {
  popuniprviput();
  console.log("UBACENO");
});
function popuniprviput() {
  db.Osoblje.create({
    ime: "Neko",
    prezime: "Nekic",
    uloga: "profesor"
  }).then(function() {
    db.Osoblje.create({
      ime: "Drugi",
      prezime: "Neko",
      uloga: "asistent"
    }).then(function() {
      db.Osoblje.create({
        ime: "Test",
        prezime: "Test",
        uloga: "asistent"
      }).then(function() {
        db.Sala.create({
          naziv: "1-11",
          zaduzenaOsoba: 1
        }).then(function() {
          db.Sala.create({
            naziv: "1-15",
            zaduzenaOsoba: 2
          }).then(function() {
            db.Termin.create({
              redovni: false,
              dan: null,
              datum: "01.01.2020",
              semestar: null,
              pocetak: "12:00",
              kraj: "13:00"
            }).then(function() {
              db.Termin.create({
                redovni: true,
                dan: 0,
                datum: null,
                semestar: "zimski",
                pocetak: "13:00",
                kraj: "14:00"
              }).then(function() {
                db.Rezervacija.create({
                  termin: 1,
                  sala: 1,
                  osoba: 1
                }).then(function() {
                  db.Rezervacija.create({
                    termin: 2,
                    sala: 1,
                    osoba: 3
                  });
                });
              });
            });
          });
        });
      });
    });
  });
}
function dajrezervacije() {
  let odg = {};
  odg.periodicna = [];
  odg.vanredna = [];
  let zauzecaListaPromisea = [];
  return db.Rezervacija.findAll().then(function(zauzeca) {
    zauzeca.forEach((item, i) => {
      let objekatOsoba = item.getOsoblje();
      let objekatTermin = item.getTermin();
      let objekatSala = item.getSala();
      zauzecaListaPromisea.push(
        Promise.all([objekatOsoba, objekatTermin, objekatSala]).then(function(
          x
        ) {
          return new Promise(function(resolve, reject) {
            resolve(x);
          });
        })
      );
    });
    return Promise.all(zauzecaListaPromisea).then(function(listaZauzeca) {
      let nizGlavni = JSON.parse(JSON.stringify(listaZauzeca));
      nizGlavni.forEach((z, i) => {
        let rezultat = {};
        z.forEach((item, i) => {
          if (item.hasOwnProperty("ime")) {
            rezultat.predavac = item.ime + " " + item.prezime;
          }
          if (item.hasOwnProperty("naziv")) rezultat.naziv = item.naziv;
          if (item.hasOwnProperty("redovni")) {
            rezultat.pocetak = item.pocetak.substr(0, 5);
            rezultat.kraj = item.kraj.substr(0, 5);
            if (item.redovni) {
              rezultat.dan = item.dan;
              rezultat.semestar = item.semestar;
              odg.periodicna.push(rezultat);
            } else {
              rezultat.datum = item.datum;
              odg.vanredna.push(rezultat);
            }
          }
        });
      });
      return new Promise(function(resolve, reject) {
        resolve(odg);
      });
    });
  });
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname));
app.get("/", function(req, res) {
  res.sendFile(__dirname + "/pocetna.html");
});
app.get("/zauzeca", function(req, res) {
  console.log("blaba");
  dajrezervacije().then(function(rezervacije) {
    console.log(rezervacije);
    res.send(rezervacije);
  });
});
app.get("/slajbe", function(req, res) {
  var prvetri = [];
  doklesam = 0;
  fs.readdir("./images", function(err, slikice) {
    for (let i = 0; i < 3; i++) {
      prvetri.push("http://localhost:8080/images/" + slikice[i]);
    }
    res.json(prvetri);
    doklesam += 3;
  });
});
app.get("/slajbe2", function(req, res) {
  var sljedecetri = [];
  var kraj = false;
  var velicina = doklesam + 3;
  fs.readdir("./images", function(err, slikice) {
    if (doklesam + 3 >= slikice.length) {
      kraj = true;
      velicina = slikice.length;
    }

    for (let i = doklesam; i < velicina; i++) {
      sljedecetri.push("http://localhost:8080/images/" + slikice[i]);
    }
    let din = { sljedecetri, kraj };
    res.json(din);
    doklesam += 3;
  });
});
app.get("/osoblje", function(req, res) {
  db.Osoblje.findAll().then(function(covjek) {
    var ljudi = [];
    for (let i = 0; i < covjek.length; i++) {
      ljudi.push(covjek[i].ime + " " + covjek[i].prezime);
    }
    res.send(ljudi);
  });
});
app.get("/osobe", function(req, res) {
  dajrezervacije().then(function(zauzeca) {
    db.Osoblje.findAll().then(function(covjek) {
      var ljudi = [];
      for (let i = 0; i < covjek.length; i++) {
        ljudi.push(covjek[i].ime + " " + covjek[i].prezime);
      }
      var objekat = { zauzeca, ljudi };
      console.log(objekat);
      res.send(objekat);
    });
  });
});
app.get("/sale", function(req, res) {
  db.Sala.findAll().then(function(sale) {
    var niz = [];
    for (let i = 0; i < sale.length; i++) {
      niz.push(sale[i].naziv);
    }
    console.log(niz);
    res.send(niz);
  });
});
app.post("/kalendar", function(req, res) {
  var tijelo = req.body;
  /*fs.readFile('./zauzeca.json',function(err,data){
        content=JSON.parse(data);*/
  dajrezervacije().then(function(content) {
    let kojezauzeo = "";
    if (Object.keys(tijelo).length == 5) {
      var prolazi = true;
      for (var l = 0; l < content.vanredna.length; l++) {
        if (
          tijelo.datum == content.vanredna[l].datum &&
          tijelo.naziv == content.vanredna[l].naziv &&
          ((Number(tijelo.pocetak.substring(0, 2)) * 60 +
            Number(tijelo.pocetak.substring(3, 5)) >=
            Number(content.vanredna[l].pocetak.substring(0, 2)) * 60 +
              Number(content.vanredna[l].pocetak.substring(3, 5)) &&
            Number(tijelo.pocetak.substring(0, 2)) * 60 +
              Number(tijelo.pocetak.substring(3, 5)) <=
              Number(content.vanredna[l].kraj.substring(0, 2)) * 60 +
                Number(content.vanredna[l].kraj.substring(3, 5))) ||
            (Number(tijelo.kraj.substring(0, 2)) * 60 +
              Number(tijelo.kraj.substring(3, 5)) >=
              Number(content.vanredna[l].pocetak.substring(0, 2)) * 60 +
                Number(content.vanredna[l].pocetak.substring(3, 5)) &&
              Number(tijelo.kraj.substring(0, 2)) * 60 +
                Number(tijelo.kraj.substring(3, 5)) <=
                Number(content.vanredna[l].kraj.substring(0, 2)) * 60 +
                  Number(content.vanredna[l].kraj.substring(3, 5))) ||
            (Number(tijelo.pocetak.substring(0, 2)) * 60 +
              Number(tijelo.pocetak.substring(3, 5)) <=
              Number(content.vanredna[l].pocetak.substring(0, 2)) * 60 +
                Number(content.vanredna[l].pocetak.substring(3, 5)) &&
              Number(tijelo.kraj.substring(0, 2)) * 60 +
                Number(tijelo.kraj.substring(3, 5)) >=
                Number(content.vanredna[l].kraj.substring(0, 2)) * 60 +
                  Number(content.vanredna[l].kraj.substring(3, 5))))
        ) {
          prolazi = false;
          kojezauzeo = content.vanredna[l].predavac;
        }
      }
      for (var o = 0; o < content.periodicna.length; o++) {
        if (
          tijelo.naziv == content.periodicna[o].naziv &&
          ((content.periodicna[o].semestar == "ljetni" &&
            Number(tijelo.datum.substring(3, 5)) - 1 >= 1 &&
            Number(tijelo.datum.substring(3, 5)) - 1 <= 5) ||
            (content.periodicna[o].semestar == "zimski" &&
              ((Number(tijelo.datum.substring(3, 5)) - 1 >= 9 &&
                Number(tijelo.datum.substring(3, 5)) - 1 <= 11) ||
                Number(tijelo.datum.substring(3, 5)) - 1 == 0))) &&
          (Number(tijelo.datum.substring(0, 2)) +
            poceci3[Number(tijelo.datum.substring(3, 5)) - 1] -
            1) %
            7 ==
            content.periodicna[o].dan &&
          ((Number(tijelo.pocetak.substring(0, 2)) * 60 +
            Number(tijelo.pocetak.substring(3, 5)) >=
            Number(content.periodicna[o].pocetak.substring(0, 2)) * 60 +
              Number(content.periodicna[o].pocetak.substring(3, 5)) &&
            Number(tijelo.pocetak.substring(0, 2)) * 60 +
              Number(tijelo.pocetak.substring(3, 5)) <=
              Number(content.periodicna[o].kraj.substring(0, 2)) * 60 +
                Number(content.periodicna[o].kraj.substring(3, 5))) ||
            (Number(tijelo.kraj.substring(0, 2)) * 60 +
              Number(tijelo.kraj.substring(3, 5)) >=
              Number(content.periodicna[o].pocetak.substring(0, 2)) * 60 +
                Number(content.periodicna[o].pocetak.substring(3, 5)) &&
              Number(tijelo.kraj.substring(0, 2)) * 60 +
                Number(tijelo.kraj.substring(3, 5)) <=
                Number(content.periodicna[o].kraj.substring(0, 2)) * 60 +
                  Number(content.periodicna[o].kraj.substring(3, 5))) ||
            (Number(tijelo.pocetak.substring(0, 2)) * 60 +
              Number(tijelo.pocetak.substring(3, 5)) <=
              Number(content.periodicna[o].pocetak.substring(0, 2)) * 60 +
                Number(content.periodicna[o].pocetak.substring(3, 5)) &&
              Number(tijelo.kraj.substring(0, 2)) * 60 +
                Number(tijelo.kraj.substring(3, 5)) >=
                Number(content.periodicna[o].kraj.substring(0, 2)) * 60 +
                  Number(content.periodicna[o].kraj.substring(3, 5))))
        ) {
          prolazi = false;
          kojezauzeo = content.periodicna[o].predavac;
        }
      }
      if (prolazi) {
        var imeiprezime = tijelo.predavac.split(" ");
        db.Termin.create({
          redovni: false,
          dan: null,
          datum: tijelo.datum,
          semestar: null,
          pocetak: tijelo.pocetak,
          kraj: tijelo.kraj
        }).then(function(termin) {
          db.Sala.findOne({ where: { naziv: tijelo.naziv } }).then(function(
            sala
          ) {
            db.Osoblje.findOne({
              where: { ime: imeiprezime[0], prezime: imeiprezime[1] }
            }).then(function(profesor) {
              db.Rezervacija.create({
                termin: termin.id,
                osoba: profesor.id,
                sala: sala.id
              });
            });
          });
        });
        content.vanredna.push(tijelo);
        res.json(content);
        /*var response="";
                content.vanredna.push(tijelo);
                var response1=response+"{"+"\n"+"\"periodicna\":["+"\n";
                for(var t=0;t<content.periodicna.length-1;t++){
                    response1=response1+JSON.stringify(content.periodicna[t])+","+"\n";
                }
                response1=response1+JSON.stringify(content.periodicna[content.periodicna.length-1])+"\n"+"        ],\n\"vanredna\":[\n";
                for(var t=0;t<content.vanredna.length-1;t++){
                    response1=response1+JSON.stringify(content.vanredna[t])+","+"\n";
                }
                response1=response1+JSON.stringify(content.vanredna[content.vanredna.length-1])+"\n        ]\n}";
                fs.writeFile('./zauzeca.json',response1,function(err){
                res.json(content);
                });*/
      } else {
        response =
          "Nije moguće rezervisati salu " +
          tijelo.naziv +
          " za navedeni datum " +
          tijelo.datum.substr(0, 2) +
          "/" +
          tijelo.datum.substr(3, 2) +
          "/" +
          tijelo.datum.substr(6, 4) +
          " i termin od " +
          tijelo.pocetak +
          " do " +
          tijelo.kraj +
          " jer je rezervisao " +
          kojezauzeo +
          "!";
        res.json(response);
      }
    } else {
      var prolazi2 = true;
      for (var q = 0; q < content.vanredna.length; q++) {
        if (
          (Number(content.vanredna[q].datum.substring(0, 2)) +
            poceci3[Number(content.vanredna[q].datum.substring(3, 5)) - 1] -
            1) %
            7 ==
            tijelo.dan &&
          tijelo.naziv == content.vanredna[q].naziv &&
          ((tijelo.semestar == "ljetni" &&
            Number(content.vanredna[q].datum.substring(3, 5)) - 1 >= 1 &&
            Number(content.vanredna[q].datum.substring(3, 5)) - 1 <= 5) ||
            (tijelo.semestar == "zimski" &&
              ((Number(content.vanredna[q].datum.substring(3, 5)) - 1 >= 9 &&
                Number(content.vanredna[q].datum.substring(3, 5)) - 1 <= 11) ||
                Number(content.vanredna[q].datum.substring(3, 5)) - 1 == 0))) &&
          ((Number(tijelo.pocetak.substring(0, 2)) * 60 +
            Number(tijelo.pocetak.substring(3, 5)) >=
            Number(content.vanredna[q].pocetak.substring(0, 2)) * 60 +
              Number(content.vanredna[q].pocetak.substring(3, 5)) &&
            Number(tijelo.pocetak.substring(0, 2)) * 60 +
              Number(tijelo.pocetak.substring(3, 5)) <=
              Number(content.vanredna[q].kraj.substring(0, 2)) * 60 +
                Number(content.vanredna[q].kraj.substring(3, 5))) ||
            (Number(tijelo.kraj.substring(0, 2)) * 60 +
              Number(tijelo.kraj.substring(3, 5)) >=
              Number(content.vanredna[q].pocetak.substring(0, 2)) * 60 +
                Number(content.vanredna[q].pocetak.substring(3, 5)) &&
              Number(tijelo.kraj.substring(0, 2)) * 60 +
                Number(tijelo.kraj.substring(3, 5)) <=
                Number(content.vanredna[q].kraj.substring(0, 2)) * 60 +
                  Number(content.vanredna[q].kraj.substring(3, 5))) ||
            (Number(tijelo.pocetak.substring(0, 2)) * 60 +
              Number(tijelo.pocetak.substring(3, 5)) <=
              Number(content.vanredna[q].pocetak.substring(0, 2)) * 60 +
                Number(content.vanredna[q].pocetak.substring(3, 5)) &&
              Number(tijelo.kraj.substring(0, 2)) * 60 +
                Number(tijelo.kraj.substring(3, 5)) >=
                Number(content.vanredna[q].kraj.substring(0, 2)) * 60 +
                  Number(content.vanredna[q].kraj.substring(3, 5))))
        ) {
          prolazi2 = false;
          kojezauzeo = content.vanredna[q].predavac;
        }
      }
      for (var f = 0; f < content.periodicna.length; f++) {
        if (
          tijelo.naziv == content.periodicna[f].naziv &&
          tijelo.semestar == content.periodicna[f].semestar &&
          tijelo.dan == content.periodicna[f].dan &&
          ((Number(tijelo.pocetak.substring(0, 2)) * 60 +
            Number(tijelo.pocetak.substring(3, 5)) >=
            Number(content.periodicna[f].pocetak.substring(0, 2)) * 60 +
              Number(content.periodicna[f].pocetak.substring(3, 5)) &&
            Number(tijelo.pocetak.substring(0, 2)) * 60 +
              Number(tijelo.pocetak.substring(3, 5)) <=
              Number(content.periodicna[f].kraj.substring(0, 2)) * 60 +
                Number(content.periodicna[f].kraj.substring(3, 5))) ||
            (Number(tijelo.kraj.substring(0, 2)) * 60 +
              Number(tijelo.kraj.substring(3, 5)) >=
              Number(content.periodicna[f].pocetak.substring(0, 2)) * 60 +
                Number(content.periodicna[f].pocetak.substring(3, 5)) &&
              Number(tijelo.kraj.substring(0, 2)) * 60 +
                Number(tijelo.kraj.substring(3, 5)) <=
                Number(content.periodicna[f].kraj.substring(0, 2)) * 60 +
                  Number(content.periodicna[f].kraj.substring(3, 5))) ||
            (Number(tijelo.pocetak.substring(0, 2)) * 60 +
              Number(tijelo.pocetak.substring(3, 5)) <=
              Number(content.periodicna[f].pocetak.substring(0, 2)) * 60 +
                Number(content.periodicna[f].pocetak.substring(3, 5)) &&
              Number(tijelo.kraj.substring(0, 2)) * 60 +
                Number(tijelo.kraj.substring(3, 5)) >=
                Number(content.periodicna[f].kraj.substring(0, 2)) * 60 +
                  Number(content.periodicna[f].kraj.substring(3, 5))))
        ) {
          prolazi2 = false;
          kojezauzeo = content.periodicna[f].predavac;
        }
      }
      if (prolazi2) {
        var imeiprezime = tijelo.predavac.split(" ");
        db.Termin.create({
          redovni: true,
          dan: tijelo.dan,
          datum: null,
          semestar: tijelo.semestar,
          pocetak: tijelo.pocetak,
          kraj: tijelo.kraj
        }).then(function(termin) {
          db.Sala.findOne({ where: { naziv: tijelo.naziv } }).then(function(
            sala
          ) {
            db.Osoblje.findOne({
              where: { ime: imeiprezime[0], prezime: imeiprezime[1] }
            }).then(function(profesor) {
              db.Rezervacija.create({
                termin: termin.id,
                osoba: profesor.id,
                sala: sala.id
              });
            });
          });
        });
        content.periodicna.push(tijelo);
        res.json(content);
        /*var odgovor=""
                content.periodicna.push(tijelo);
                var response2=odgovor+"{"+"\n"+"\"periodicna\":["+"\n";
                for(var t=0;t<content.periodicna.length-1;t++){
                    response2=response2+JSON.stringify(content.periodicna[t])+","+"\n";
                }
                response2=response2+JSON.stringify(content.periodicna[content.periodicna.length-1])+"\n"+"        ],\n\"vanredna\":[\n";
                for(var t=0;t<content.vanredna.length-1;t++){
                    response2=response2+JSON.stringify(content.vanredna[t])+","+"\n";
                }
                response2=response2+JSON.stringify(content.vanredna[content.vanredna.length-1])+"\n        ]\n}";
                fs.writeFile('./zauzeca.json',response2,function(err){
                    res.json(content);
                });*/
      } else {
        response =
          "Nije moguće periodično zauzeti ovu salu u traženom terminu jer je rezervisao " +
          kojezauzeo +
          "!";
        res.json(response);
      }
    }
  });
});
app.listen(8080);
