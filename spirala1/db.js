const Sequelize = require("sequelize");
const sequelize = new Sequelize("DBWT19","root","root",{host:"127.0.0.1",dialect:"mysql",logging:false});
const db={};

db.Sequelize = Sequelize;  
db.sequelize = sequelize;

//import modela
db.Osoblje = sequelize.import(__dirname+'/osoblje.js');
db.Rezervacija = sequelize.import(__dirname+'/rezervacija.js');
db.Termin = sequelize.import(__dirname+'/termin.js');
db.Sala = sequelize.import(__dirname+'/sala.js');

//relacije
// Veza 1-n
db.Osoblje.hasMany(db.Rezervacija, {foreignKey:'osoba'});
db.Rezervacija.belongsTo(db.Osoblje, {foreignKey:'osoba'});

// Veza 1-1
db.Termin.hasOne(db.Rezervacija, {foreignKey:{name:'termin',unique:true, type:Sequelize.INTEGER}});
db.Rezervacija.belongsTo(db.Termin, {foreignKey:{name:'termin',unique:true, type:Sequelize.INTEGER}});

// Veza 1-n
db.Sala.hasMany(db.Rezervacija, {foreignKey:'sala'});
db.Rezervacija.belongsTo(db.Sala, {foreignKey:'sala'});

db.Osoblje.hasOne(db.Sala, {foreignKey:'zaduzenaOsoba'});
db.Sala.belongsTo(db.Osoblje, {foreignKey:'zaduzenaOsoba'});





module.exports=db;