var today = new Date();
var t = today.getMonth();
/*var periodicna=[
    { dan:0, semestar: "zimski", pocetak:"10:30", kraj:"15:00", naziv: "VA1", predavac: "blabla" },
    { dan:3, semestar: "ljetni", pocetak:"10:30", kraj:"15:00", naziv: "VA1", predavac: "blabla" },
    {dan:0, semestar: "zimski", pocetak:"10:30", kraj:"15:00", naziv: "EE1", predavac: "blabla" },
    { dan:3, semestar: "ljetni", pocetak:"10:30", kraj:"15:00", naziv: "EE1", predavac: "blabla" },
    {dan:0, semestar: "zimski", pocetak:"18:00", kraj:"20:00", naziv: "EE1", predavac: "blabla" },
    { dan:3, semestar: "ljetni", pocetak:"18:00", kraj:"20:00", naziv: "EE1", predavac: "blabla" }];
var vandredna=[
    {datum: "26.08.2019", pocetak: "09:00", kraj: "16:00", naziv: "VA1", predavac: "blabla"},
    {datum: "13.08.2019", pocetak: "09:00", kraj: "16:00", naziv: "VA1", predavac: "blabla"}];*/

let Kalendar = (function(){
    var periodicnazauzeca;
    var vandrednazauzeca;
    function obojiZauzecaImpl(kalendarRef, mjesec, sala, pocetak, kraj){
        
    for(var i=0;i<42;i++){
        document.getElementsByClassName("mala")[i].children[0].children[1].children[0].classList="slobodna";
    }
        if(pocetak=="" || kraj==""  || Number (pocetak.substring(0,2))> Number( kraj.substring(0,2)) || ((Number (pocetak.substring(0,2))== Number(kraj.substring(0,2))) && (Number (pocetak.substring(3,5))>Number(kraj.substring(3,5)))))
        return;
        
        
        for(var i=0;i<periodicnazauzeca.length;i++){
            if((Number(periodicnazauzeca[i].dan)<0 && Number(periodicnazauzeca[i].dan)>6) || (periodicnazauzeca[i].semestar !="ljetni" && periodicnazauzeca[i].semestar!="zimski"))
            continue;
            if(((periodicnazauzeca[i].semestar=='ljetni' && mjesec>=1 && mjesec<=5) || (periodicnazauzeca[i].semestar=='zimski' && 
            ((mjesec>=9 && mjesec<=11)|| mjesec==0))) && periodicnazauzeca[i].naziv==sala){
                if(( ( (Number ( pocetak.substring(0,2) ) )*60+(Number(pocetak.substring(3,5) ) ) )>=( ( Number( periodicnazauzeca[i].pocetak.substring(0,2) ) )*60+Number(periodicnazauzeca[i].pocetak.substring(3,5)))) && 
                    (( (Number ( pocetak.substring(0,2) ) )*60+(Number(pocetak.substring(3,5) ) ) )<=( ( Number( periodicnazauzeca[i].kraj.substring(0,2) ) )*60+Number(periodicnazauzeca[i].kraj.substring(3,5))))
                    || (( ( (Number ( kraj.substring(0,2) ) )*60+(Number(kraj.substring(3,5) ) ) )>=( ( Number( periodicnazauzeca[i].pocetak.substring(0,2) ) )*60+Number(periodicnazauzeca[i].pocetak.substring(3,5)))) && 
                    (( (Number ( kraj.substring(0,2) ) )*60+(Number(kraj.substring(3,5) ) ) )<=( ( Number( periodicnazauzeca[i].kraj.substring(0,2) ) )*60+Number(periodicnazauzeca[i].kraj.substring(3,5)))))
                    || (( ( (Number ( pocetak.substring(0,2) ) )*60+(Number(pocetak.substring(3,5) ) ) )<=( ( Number( periodicnazauzeca[i].pocetak.substring(0,2) ) )*60+Number(periodicnazauzeca[i].pocetak.substring(3,5)))) && 
                    (( (Number ( kraj.substring(0,2) ) )*60+(Number(kraj.substring(3,5) ) ) )>=( ( Number(periodicnazauzeca[i].kraj.substring(0,2) ) )*60+Number(periodicnazauzeca[i].kraj.substring(3,5)))))){
                    for(var j=periodicnazauzeca[i].dan;j<42;j+=7){
                        document.getElementsByClassName("mala")[j].children[0].children[1].children[0].classList="zauzeta";
                    }
                }
            }
        }
        for(var i=0;i<vandrednazauzeca.length;i++){
            if((Number(vandrednazauzeca[i].datum.substring(3,5))-1)==mjesec && vandrednazauzeca[i].naziv==sala){
                if(( ( (Number ( pocetak.substring(0,2) ) )*60+(Number(pocetak.substring(3,5) ) ) )>=( ( Number( vandrednazauzeca[i].pocetak.substring(0,2) ) )*60+Number(vandrednazauzeca[i].pocetak.substring(3,5)))) && 
                    (( (Number ( pocetak.substring(0,2) ) )*60+(Number(pocetak.substring(3,5) ) ) )<=( ( Number( vandrednazauzeca[i].kraj.substring(0,2) ) )*60+Number(vandrednazauzeca[i].kraj.substring(3,5))))
                    || (( ( (Number ( kraj.substring(0,2) ) )*60+(Number(kraj.substring(3,5) ) ) )>=( ( Number( vandrednazauzeca[i].pocetak.substring(0,2) ) )*60+Number(vandrednazauzeca[i].pocetak.substring(3,5)))) && 
                    (( (Number ( kraj.substring(0,2) ) )*60+(Number(kraj.substring(3,5) ) ) )<=( ( Number( vandrednazauzeca[i].kraj.substring(0,2) ) )*60+Number(vandrednazauzeca[i].kraj.substring(3,5)))))
                    || (( ( (Number ( pocetak.substring(0,2) ) )*60+(Number(pocetak.substring(3,5) ) ) )<=( ( Number( vandrednazauzeca[i].pocetak.substring(0,2) ) )*60+Number(vandrednazauzeca[i].pocetak.substring(3,5)))) && 
                    (( (Number ( kraj.substring(0,2) ) )*60+(Number(kraj.substring(3,5) ) ) )>=( ( Number( vandrednazauzeca[i].kraj.substring(0,2) ) )*60+Number(vandrednazauzeca[i].kraj.substring(3,5)))))){
                    var dan=Number(vandrednazauzeca[i].datum.substring(0,2))-1;
                    var x=0;
                    while(document.getElementsByClassName("mala")[x].classList.contains("nevidljiva"))
                    x++;
                    document.getElementsByClassName("mala")[x+dan].children[0].children[1].children[0].classList="zauzeta";
                }
            }
        }
    }
    function ucitajPodatkeImpl(periodicna, vandredna){
        periodicnazauzeca=periodicna;
        vandrednazauzeca=vandredna;
    }
    function iscrtajKalendarImpl(kalendarRef, mjesec){
        if(mjesec==0)
        {
            document.getElementById("primaknut").disabled=true;
        }
        else if(mjesec==11){
            document.getElementById("odmaknut").disabled=true;
        }
        else{
            document.getElementById("odmaknut").disabled=false;
            document.getElementById("primaknut").disabled=false;
        }
        var poceci=[2,5,6,2,4,0,2,5,1,3,6,1];
        var mjeseci=['Januar','Februar','Mart','April','Maj','Juni','Juli','August','Septembar','Oktobar','Novembar','Decembar'];
        var brojevi=[31,29,31,30,31,30,31,31,30,31,30,31];
        document.getElementById("mjesec").textContent=mjeseci[mjesec];
        var k=1;
        for(var i=0;i<42;i++){
            document.getElementsByClassName("mala")[i].classList.remove("nevidljiva");
        }
        for(var i=0;i<poceci[mjesec];i++){
            document.getElementsByClassName("mala")[i].classList.add("nevidljiva");
        }
        for(var i=poceci[mjesec];i<poceci[mjesec]+brojevi[mjesec];i++){
            document.getElementsByClassName("gornja")[i].innerHTML=k;
            k++;
        }
        for(var i=poceci[mjesec]+brojevi[mjesec];i<42;i++){
            document.getElementsByClassName("mala")[i].classList.add("nevidljiva");
        }

    }
    return {
    obojiZauzeca: obojiZauzecaImpl,
    ucitajPodatke: ucitajPodatkeImpl,
    iscrtajKalendar: iscrtajKalendarImpl
    }
    }());
    